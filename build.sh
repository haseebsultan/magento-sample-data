# #!/usr/bin/env bash
# set -e

# source "build/bin/config"
# source "build/bin/vcs-changed-files"

# function ci:static:phpcs(){
#     vcs-changed-files
#     [[ -z "${CHANGED_FILES}" ]] && echo "no changed files" && return
#     ${PHP_BIN} ${PHPCS_BIN} --config-set installed_paths ../../slevomat/coding-standard,../../magento/magento-coding-standard/
#     ${PHP_BIN} ${PHPCS_BIN} --standard=phpcs-ruleset.xml ${CHANGED_FILES}
# }

# function ci:static:phpcbf(){
#     vcs-changed-files
#     [[ -z "${CHANGED_FILES}" ]] && echo "no changed files" && return
#     ${PHP_BIN} ${PHPCS_BIN} --config-set installed_paths ../../slevomat/coding-standard,../../magento/magento-coding-standard/
#     ${PHP_BIN} ${PHPCBF_BIN} --standard=phpcs-ruleset.xml ${CHANGED_FILES} || true
#     [[ $(${GIT_BIN} status --porcelain --untracked-files=no | wc -l) -eq 0 ]] && echo "no changed files" && return
#     ${GIT_BIN} status --porcelain --untracked-files=no
#     [[ ${PHPCBF_GIT_PUSH} -eq 0 ]] && echo "not commiting changes" && return
#     # push needs to happen here because of the early exit above
#     printf "ADDING FILES CHANGED_FILES\n"
#     ${GIT_BIN} add -u
#     ${GIT_BIN} status
#     printf "COMMITING CHANGED_FILES\n"
#     ${GIT_BIN} commit -m 'PHP Code Style Update'
#     ${GIT_BIN} status
#     printf "PUSH CHANGED_FILES\n"
#     ${GIT_BIN} push -o ci.skip origin ${CI_COMMIT_BRANCH}
# }

# function ci:static:phpmd(){
#     vcs-changed-files
#     PHPMD_DIRS=$(echo ${CHANGED_FILES} | tr ' ' ',')
#     printf "PHPMD DIRS/FILES:\n${PHPMD_DIRS}\n"
#     php ${PHPMD_BIN} ${PHPMD_DIRS} text phpmd-ruleset.xml --exclude "Test" || true
#     php ${PHPMD_BIN} ${PHPMD_DIRS} html phpmd-ruleset.xml --exclude "Test" --reportfile build/output/phpmd/phpmd.html
# }

# function ci:unit(){
#     echo "---------- UNIT TESTS PHPUNIT ----------"
#     ${PHP_BIN} vendor/bin/phpunit -c phpunit.xml.dist  --coverage-text --colors=never --disable-coverage-ignore
# }

# function ci:integration(){
#     echo "---------- MAGENTO CLI CHECK ----------"
#     ${PHP_BIN} bin/magento

#     magento:setup:db

#     echo "---------- INTEGRATION TESTS CONFIG ----------"
#     if [[ -f ${PROJECT_ROOT}/dev/tests/integration/etc/install-config-mysql.php ]]; then
#       rm dev/tests/integration/etc/install-config-mysql.php
#     fi
#     if [[ ! -f ${PROJECT_ROOT}/dev/tests/integration/etc/install-config-mysql.php ]]; then
#       envsubst < build/tests/integration/install-config-mysql.php > dev/tests/integration/etc/install-config-mysql.php
#     fi
#     cp dev/tests/integration/etc/config-global.php.dist dev/tests/integration/etc/config-global.php

#     # run tests
#     echo "---------- INTEGRATION TESTS PHPUNIT ----------"
#     cd dev/tests/integration || return;
#     ${PHP_BIN} ${PROJECT_ROOT}/vendor/bin/phpunit -c ${PROJECT_ROOT}/phpunit-integration.xml.dist --no-coverage --colors=never --disable-coverage-ignore
# }

# function magento:setup:db(){
#     # install magento if not installed
#     if [[ ! -f ${PROJECT_ROOT}/app/etc/env.php ]]; then
#         echo "---------- MAGENTO SETUP:INSTALL ----------"
#         ${PHP_BIN} bin/magento setup:install --db-host=${APP_DB_HOST} --db-name=${APP_DB1_NAME} --db-password=${APP_DB_PASS} --db-user=${APP_DB_USER} --admin-email=admin@mwltr.de --admin-firstname=Admin --admin-lastname=Admin --admin-password=admin123 --admin-user=admin --backend-frontname=admin --base-url=http://rar.m2-prometheus.test --currency=EUR --language=en_US --session-save=files --timezone=Europe/Berlin --use-rewrites=1
#     fi
#     echo "---------- MAGENTO SETUP:UPGRADE ----------"
#     ${PHP_BIN} bin/magento setup:upgrade
# }

# function ci:smoke(){
#     # run tests
#     echo "---------- SMOKE TESTS CIGAR ----------"
#     CIGAR_CONFIG=${PROJECT_ROOT}/build/tests/smoke/.cigar.$1.json
#     if ! test -f "$CIGAR_CONFIG"; then
#       echo "$CIGAR_CONFIG does not exist."
#       exit
#     fi
#     ${PHP_BIN} ${PROJECT_ROOT}/build/tools/bin/cigar --config ${CIGAR_CONFIG}
# }

# function ci:docs(){
#     echo "---------- DOCS ----------"
#     # Symlink READMEs to docs
#     echo "---------- DOCS SYMLINK README.mds TO docs ----------"
#     MODULE_DIRS=$(find ${SOURCE_DIR_LIST} -name 'README.md')
#     for filePath in ${MODULE_DIRS[@]}
#     do
#         modulePath=$(dirname ${filePath})
#         moduleDir=$(basename ${modulePath})
#         namespaceDir=$(basename $(dirname "${modulePath}"))
#         fileName=$(basename ${filePath})
#         docsFilePath="docs/${namespaceDir}_${moduleDir}/${fileName}"
#         mkdir -p "docs/${namespaceDir}_${moduleDir}"

#         if [[ ! -f ${docsFilePath} ]]; then
#             echo "${filePath} -> ${docsFilePath}"
#             # Create Symlink to docs dir if missing
#             [[ ! -f "${docsFilePath}" ]] && ln -s "${PROJECT_ROOT}/${filePath}" "${docsFilePath}"
#         fi
#     done

#     # Symlink doc dirs to docs/module
#     echo "---------- DOCS SYMLINK docs subfolders to module IN docs ----------"
#     MODULE_DOC_DIRS=$(find ${SOURCE_DIR_LIST} -name 'docs')
#     echo ${MODULE_DOC_DIRS}
#     # for dirPath in ${MODULE_DOC_DIRS[@]}
#     # do
#     #     modulePath=$(dirname ${dirPath})
#     #     moduleDir=$(basename ${modulePath})
#     #     namespaceDir=$(basename $(dirname "${modulePath}"))
#     #     fileName=$(basename ${dirPath})
#     #     docsFilePath="docs/${namespaceDir}_${moduleDir}/"
#     #     mkdir -p "docs/${namespaceDir}_${moduleDir}"

#     #     if [[ ! -f ${docsFilePath} ]]; then
#     #         echo "${dirPath} -> ${docsFilePath}"
#     #         # Create Symlink to docs dir if missing
#     #         [[ ! -f "${docsFilePath}" ]] && ln -s "${PROJECT_ROOT}/${dirPath}" "${docsFilePath}"
#     #     fi
#     # done

#     # Symlink project README
#     echo "---------- DOCS SYMLINK PROJECT README.md TO docs ----------"
#     [[ ! -f "docs/README.md" ]] && ln -s "${PROJECT_ROOT}/README.md" "docs/README.md"

#     # Clone theme
#     echo "---------- DOCS CLONE MKDOCS THEME ----------"
#     themeDir="docs/templates/mkdocs-material"
#     [[ ! -d ${themeDir} ]] && git clone --depth=1 --branch ${DOCS_THEME_VERSION} ${DOCS_THEME_GIT_URL} ${themeDir}

#     # Build docs
#     echo "---------- DOCS BUILD MKDOCS ----------"
#     mkdocs build
# }

# function ci:artifact-generate(){
#     # generate supervisord config
#     supervisord:config-generate

#     # Magento compilation
#     ${PHP_BIN} bin/magento setup:di:compile || exit 1
#     ${PHP_BIN} bin/magento setup:static-content:deploy -f -s compact --area adminhtml ${LOCALES_ADMIN} || exit 1
#     ${PHP_BIN} bin/magento setup:static-content:deploy -f -s compact --area frontend --theme ${FRONTEND_THEME} ${LOCALES_FRONTEND} || exit 1

#     # create artifact
#     mkdir -p ${PROJECT_ROOT}/build/artifacts
#     ${TAR_BIN} --exclude-vcs --checkpoint=5000 --exclude-from=.artifact.ignore -czf ${PROJECT_ROOT}/build/artifacts/shop.tar.gz . || exit 1
# }

# function supervisord:config-generate(){
#     declare -A instances=(['dev']='dev1;dev2' ['staging']='staging')
#     for env in "${!instances[@]}"; do
#         regionData=${instances[$env]};
#         res="${regionData//[^;]}" # grep all ; occurences in regions
#         regionCount=${#res} # count if there are regional instances on this environment (multi region environment)
#         regions=$(echo ${regionData} | tr ";" "\n");
#         for regionId in ${regions} ; do
#             logDir="/mnt/disk00/logs"
#             rootDir="/mnt/disk00/www"
#             if [[ "${regionCount}" -gt "0" ]]; then
#                 logDir="${logDir}/${regionId}"
#                 rootDir="${rootDir}-${regionId}"
#             fi

#             # exporting current values so envsubst will pick them up and replace them in the supervisor.tmpl.ini
#             export RID=${regionId}
#             export SUPERVISORD_LOG_DIR=${logDir}
#             export SUPERVISORD_ROOT_DIR=${rootDir}

#             # Generate the file
#             supervisordFile="config/supervisord/${env}/supervisord-${regionId}.ini"
#             envsubst < config/supervisord/supervisord.tmpl.ini > ${supervisordFile}
#         done
#     done
# }

# printHelp=1
# IFS=$'\n'
# SCRIPT_FUNCTIONS=$(declare -F)

# # Find and execute functions
# for f in ${SCRIPT_FUNCTIONS}; do
#     action=${f:11}
#     if [[ ${action} == $1 ]]; then
#         printHelp=0
#         IFS=$' \t\n' #reset IFS before executing the action this might have side-effects
#         ${action}
#     fi
# done

# # print help in case no action was executed
# if [[ ${printHelp} == 1 ]]; then
#     for f in ${SCRIPT_FUNCTIONS}; do
#         if echo ${f:11} | grep -q "_"; then
#             continue;
#         fi

#         echo "${f:11}"
#     done
# fi